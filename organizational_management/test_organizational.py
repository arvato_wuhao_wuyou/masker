import json
import sys

import pytest
from get_yaml.analysis_yaml import StartYaml
from request_file.send_request import RequestUtil


def getheader(header):
    yml_dick = StartYaml().read_url_headers_yaml(header)
    return yml_dick


def geturl(url):
    url_dick = StartYaml().read_url_headers_yaml(url)['url']
    return url_dick


def use_disable(url, methon, headers, data):
    reponse_disable = RequestUtil().send_qequest(url=url, methon=methon, headers=headers, data=data)
    reponse_disable_json = reponse_disable.json()
    assert reponse_disable_json['code'] == 0
    assert reponse_disable_json['msg'] == "成功"


class TestOrganizationalMang:
    # 调用方法获取请求头和url
    headers = getheader("D:/LXDZ_pytest/header.yml")
    url = geturl("D:/LXDZ_pytest/url.yml")
    # 先请求列表页接口获取父机构id并将Id替换给到select.yaml文件中的id，否则会出现报错
    data = {}
    get_organizational_list = RequestUtil().send_qequest(url="http://192.168.41.12/admin/dept/tree", methon="GET",
                                                         headers=headers, data=data)
    get_organizational_list_json = get_organizational_list.json()
    deptId = get_organizational_list_json['data'][0]['id']
    sel_yml_path = StartYaml().read_yaml("select.yml")
    sel_yml_path[
        'path'] = "/admin/dept/getDescendantListPage?deptId=" + deptId + "&name=&deptType=&deptState=&current=1&size=10&descs=&ascs="
    # 必须调用清除引号的方法写入函数，不然会在yaml里面写入引号，从而获取不到数据
    StartYaml().write_yaml_yinhao_list("select.yml", sel_yml_path)
    # 读取新建的yaml文件，并且请求查询接口自动将父组织机构id替换到新建yaml文件中
    sel_yml = StartYaml().read_yaml("select.yml")
    yaml_data = StartYaml().read_yaml("add_organizational.yml")
    sel_rep = RequestUtil().send_qequest(url=url + sel_yml["path"], methon="GET", data=sel_yml["data"], headers=headers)
    sel_rep_json = sel_rep.json()
    sel_rep_yaml = sel_rep_json['data']['records'][0]['deptId']
    for item in yaml_data:
        item['request']["parentId"] = sel_rep_yaml
    StartYaml().write_yaml_list('add_organizational.yml', yaml_data)

    # 该方法必须在类中调用因为请求参数为类变量
    def select_user(self):
        sel_rep = RequestUtil().send_qequest(url=self.url + self.sel_yml["path"], methon="GET",
                                             data=self.sel_yml["data"], headers=self.headers)
        sel_rep_json = sel_rep.json()
        return sel_rep_json

    # 创建组织机构测试
    @pytest.mark.parametrize("info", StartYaml().read_yaml("add_organizational.yml"))
    def test_add_organizationa(self, info):
        # 判断组织机构是否是档口（注意yaml文件中需保证在档口数据创建之前有食堂数据，否则会导致报错）
        if info["request"]["deptType"] == 3:
            # 是档口的机构需要请求查询，将所有的组织机构数据查询之后将查询出来的ID给到机构创建的Yaml文件中将deptId替换
            sel_rep = RequestUtil().send_qequest(url=self.url + self.sel_yml["path"], methon="GET",
                                                 data=self.sel_yml["data"], headers=self.headers)

            sel_rep_json = sel_rep.json()
            # 断言查询界面是否请求成功且数据是否为空
            assert sel_rep_json["code"] == 0 and sel_rep_json["data"] is not None
            # 循环遍历查询接口的响应体，取出parentId并替换为deptId
            for deptId in sel_rep_json['data']['records']:
                if deptId['deptType'] == 1:
                    info["request"]["parentId"] = deptId['deptId']
        data = json.dumps(info["request"])
        path = info["path"]
        # 发起创建组织机构请求
        rep = RequestUtil().send_qequest(url=self.url + path, methon="POST", headers=self.headers, data=data)
        re_json = rep.json()
        assert re_json['code'] == 0
        assert re_json['msg'] == "成功"

    # 单个禁用和启用
    def test_disable_use_user(self):
        path = '/admin/dept/updateStatusByIds'
        # 调用查询接口的数据获取所有机构的id
        sel_rep_json = self.select_user()
        for item in sel_rep_json['data']['records']:
            # 禁用请求体
            data1 = {
                "deptIds": [item['deptId']],
                "deptState": 1
            }
            # 启用请求体
            data2 = {
                "deptIds": [item['deptId']],
                "deptState": 0
            }
            data_disable = json.dumps(data1)
            use_disable(url=self.url + path, methon="POST", headers=self.headers, data=data_disable)
            data_use = json.dumps(data2)
            use_disable(url=self.url + path, methon="POST", headers=self.headers, data=data_use)

    # 批量启用禁用测试
    def test_batch_use_disable(self):
        path = "/admin/dept/updateStatusByIds"
        # 调用查询接口的数据获取所有机构的id
        sel_rep_json = self.select_user()
        user_dusable = []
        for item in sel_rep_json['data']['records']:
            user_dusable.append(item['deptId'])
        data1 = {
            "deptIds": user_dusable,
            "deptState": 1
        }
        data_disable = json.dumps(data1)
        use_disable(url=self.url + path, methon="POST", headers=self.headers, data=data_disable)
        data2 = {
            "deptIds": user_dusable,
            "deptState": 0
        }
        data_use = json.dumps(data2)
        use_disable(url=self.url + path, methon="POST", headers=self.headers, data=data_use)

    # 编辑接口测试
    def test_edit_organizationa(self):
        path = "/admin/dept/"
        # 调用查询接口的数据获取所有机构的id
        sel_rep_json = self.select_user()
        use_rep = sel_rep_json['data']['records'][1]
        use_rep["name"] = "编辑测试"
        use_rep["deptType"] = 1
        use_rep["areaName"] = "北京市西城区"
        use_rep["deptAddress"] = "金融中心"
        use_rep["openingHoursStart"] = "08:30:00"
        use_rep["openingHoursEnd"] = "17:30:00"
        use_rep['areaCode'] = "110000,110100,110101"
        use_rep_json = json.dumps(use_rep)
        rep = RequestUtil().send_qequest(url=self.url + path, methon="PUT", headers=self.headers, data=use_rep_json)
        rep_json = rep.json()
        assert rep_json['code'] == 0
        assert rep_json['msg'] == "成功"

    # 删除组织机构和批量删除测试
    def test_delect_organizationa(self):
        path = "/admin/dept/"
        batch_path = "/admin/dept/removeById"
        # 获取列表中所有组织机构的id
        delete_batch_id = []
        deleteid = 1
        sel_rep_json = self.select_user()
        for item in sel_rep_json['data']['records']:
            if item['name'] == '删除机构测试':
                deleteid = item['deptId']
            if item['name'] == '批量删除机构测试':
                delete_batch_id.append(str(item['deptId']))
        data = {

        }
        rep = RequestUtil().send_qequest(url=self.url + path + str(deleteid), methon="DELETE", headers=self.headers,
                                         data=data)
        rep_json = rep.json()
        assert rep_json['code'] == 0
        assert rep_json['msg'] == "成功"
        batch_rep = RequestUtil().send_qequest(url=self.url + batch_path, methon="POST", headers=self.headers,
                                               data=delete_batch_id)
        batch_rep_json = batch_rep.json()
        assert batch_rep_json['code'] == 0
        assert batch_rep_json['msg'] == "成功"

    # 查询组织机构测试
    def test_select_organizationa(self):
        path = "/admin/dept/getDescendantListPage"
        deptId = self.deptId
        query_params = {
            'deptId': deptId,
            'name': '测试',
            'deptType': '0',
            'deptState': '0',
            'current': '1',
            'size': '10',
            'descs': '',
            'ascs': ''
        }
        header_raw = self.headers
        # 因为上传的get数据没有请求头Content-Type所以使用pop方法删除请求头中的Content-Type
        header_raw.pop("Content-Type")
        batch_rep = RequestUtil().send_qequest(url=self.url + path, methon="GET", headers=header_raw, data=query_params)
        batch_rep_json = batch_rep.json()
        for item in batch_rep_json['data']['records']:
            assert "测试" in item['name']
            assert item['deptType'] == 0
