import requests


class RequestUtil:
    session = requests.Session()

    def send_qequest(self, methon, url, data, **kwargs):
        # #将methon转化为小写
        # methon =str(methon).lower()

        reponse = None
        if methon == "GET":
            reponse = self.session.get(url=url, data=data, **kwargs)
            return reponse
        elif methon == "POST":
            # 判断当传入的参数不是json是列表的时候上传参数使用json上传
            if isinstance(data, list):
                reponse = self.session.post(url=url, json=data, **kwargs)
                return reponse
            else:
                reponse = self.session.post(url=url, data=data, **kwargs)
                return reponse
        elif methon == "PUT":
            reponse = self.session.put(url=url, data=data, **kwargs)
            return reponse
        elif methon == "DELETE":
            reponse = self.session.delete(url=url, data=data, **kwargs)
            return reponse
