import os
import yaml


class StartYaml:
    # 查询当前路径下的yaml
    @staticmethod
    def read_yaml(name):
        with open(os.getcwd() + "/" + name, mode="r", encoding="utf-8") as f:
            value = yaml.load(stream=f, Loader=yaml.FullLoader)
            return value

    # 查询绝对路径下的yaml
    @staticmethod
    def read_url_headers_yaml(path):
        with open(path, mode="r", encoding="utf-8") as f:
            value = yaml.load(stream=f, Loader=yaml.FullLoader)
            return value

    # 往json数据中写入yaml
    @staticmethod
    def write_yaml_list(name, organizationaList):
        with open(os.getcwd() + "/" + name, mode="w", encoding="utf-8") as f:
            yaml.dump(organizationaList, f, allow_unicode=True, default_flow_style=False)

    # 往写入yaml不加引号
    @staticmethod
    def write_yaml_yinhao_list(name, organizationaList):
        with open(os.getcwd() + "/" + name, mode="w", encoding="utf-8") as f:
            yaml.dump(organizationaList, f, default_style=None, default_flow_style=False)
